package ua.com.testmatick.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ua.com.testmatick.pages.HomePage;
import ua.com.testmatick.pages.ItemPage;
import ua.com.testmatick.pages.CartPage;
import ua.com.testmatick.pages.ResultsPage;

import java.util.concurrent.TimeUnit;

public class ActionsWithItemSuite {

    private WebDriver chromeDriver;

    private String itemName;
    private final String emptyCartAttribute = "empty-cart-title inline sprite-side";

    @BeforeClass
    private void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:/browsersDrivers/chromedriver.exe");
        itemName = System.getProperty("itemName");
        chromeDriver = new ChromeDriver();
        chromeDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        chromeDriver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
        chromeDriver.manage().window().maximize();
        chromeDriver.get("https://rozetka.com.ua/");
    }

    @Test
    public void findItemTest() {
        HomePage homePage = new HomePage(chromeDriver);
        ResultsPage results = homePage.searchFor(itemName);
        Assert.assertTrue(results.getSearchResultTitle().trim().toLowerCase().contains(itemName.toLowerCase()));
        results.selectFirstResult();
    }

    @Test
    public void addItemToCartTest() {
        ItemPage itemPage = new ItemPage(chromeDriver);
        Assert.assertTrue(itemPage.getItemName().trim().toLowerCase().contains(itemName.toLowerCase()));
        CartPage cart = itemPage.addItemToCart();
        Assert.assertTrue(itemPage.getItemName().trim().toLowerCase().contains(itemName.toLowerCase()));
        cart.continueShopping();
        Assert.assertTrue(itemPage.getItemName().trim().toLowerCase().contains(itemName.toLowerCase()));
        itemPage.gotoCart();
        Assert.assertTrue(itemPage.getItemName().trim().toLowerCase().contains(itemName.toLowerCase()));
    }

    @Test
    public void deleteItemFromCartTest() {
        CartPage cart = new CartPage(chromeDriver);
        cart.deleteItemFromCartWithoutSave();
        Assert.assertEquals(cart.getEmptyCartAttribute("class"), emptyCartAttribute);
        cart.closeCart();
    }

    @Test
    public void verifyEmptyCartTest() {
        ItemPage itemPage = new ItemPage(chromeDriver);
        Assert.assertTrue(itemPage.getItemName().trim().toLowerCase().contains(itemName.toLowerCase()));
        CartPage cart = itemPage.gotoCart();
        Assert.assertEquals(cart.getEmptyCartAttribute("class"), emptyCartAttribute);
        cart.closeCart().refreshPage().gotoCart();
        Assert.assertEquals(cart.getEmptyCartAttribute("class"), emptyCartAttribute);
        cart.closeCart();
    }

    @AfterClass
    private void tearDown() {
        if (chromeDriver != null) {
            chromeDriver.quit();
        }
    }

}
