package ua.com.testmatick.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ItemPage extends PageObjectRozetkaBase {

    @FindBy(xpath = "//div[@class='detail-title-code pos-fix clearfix']//h1[@class='detail-title']")
    private WebElement itemName;

    @FindBy(xpath = "//div[@id='price_container']//div[@name='buy_details']//button[@class='btn-link-i']")
    private WebElement buyButton;

    @FindBy(xpath = "//li[@id='cart_popup_header']//div[@name='splash-button']//a")
    private WebElement cartButton;

    public ItemPage(WebDriver driver) {
        super(driver);
    }

    public String getItemName() {
        return itemName.getText();
    }

    public CartPage addItemToCart() {
        buyButton.click();
        return new CartPage(driver);
    }

    public ItemPage refreshPage() {
        driver.navigate().refresh();
        return this;
    }

    public CartPage gotoCart() {
        wait.until(ExpectedConditions.elementToBeClickable(cartButton));
        cartButton.click();
        return new CartPage(driver);
    }

}
