package ua.com.testmatick.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class CartPage extends PageObjectRozetkaBase {

    @FindBy(xpath = "//div[@class='clearfix cart-i cart-added active']//div[@class='cart-i-title']/a[@name='goods-link']")
    private WebElement currentItem;

    @FindBy(xpath = "//div[@class='wrap-cart-not-empty  with-cart-amount']//div[@class='cart-other']//div[@class='clearfix cart-i active']//")
    private List<WebElement> otherItems;

    @FindBy(className = "cart-check")
    private WebElement beforeDeleteItemButton;

    @FindBy(xpath = "//div[@name='popup-move']//div[@class='cart-i-delete']/a[@name='delete']")
    private WebElement deleteItemWithoutSaveButton;

    @FindBy(xpath = "//div[@class='clearfix cart-total']//a[@name='close']")
    private WebElement continueShoppingButton;

    @FindBy(xpath = "//div[@id='cart-popup']/a[@name='close']")
    private WebElement closeCartButton;

    @FindBy(xpath = "//div[@id='drop-block']/h2[@class='empty-cart-title inline sprite-side']")
    private WebElement emptyCart;

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getCurrentItem() {
        return currentItem;
    }

    public List<WebElement> getOtherItems() {
        return otherItems;
    }

    public String getEmptyCartAttribute(String attribute) {
        return emptyCart.getAttribute(attribute);
    }

    public CartPage deleteItemFromCartWithoutSave() {
        wait.until(ExpectedConditions.elementToBeClickable(beforeDeleteItemButton));
        beforeDeleteItemButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(deleteItemWithoutSaveButton));
        deleteItemWithoutSaveButton.click();
        return this;
    }

    public ItemPage continueShopping() {
        wait.until(ExpectedConditions.elementToBeClickable(continueShoppingButton)).click();
        continueShoppingButton.click();
        return new ItemPage(driver);
    }

    public ItemPage closeCart() {
        wait.until(ExpectedConditions.elementToBeClickable(closeCartButton));
        closeCartButton.click();
        return new ItemPage(driver);
    }
}
