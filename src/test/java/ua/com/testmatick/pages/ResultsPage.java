package ua.com.testmatick.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.NoSuchElementException;

public class ResultsPage extends PageObjectRozetkaBase {

    private List<WebElement> results;

    @FindBy(id = "search_result_title_text")
    private WebElement searchResultTitle;

    public ResultsPage(WebDriver driver) {
        super(driver);
    }

    public String getSearchResultTitle() {
        return searchResultTitle.getText();
    }

    public ItemPage selectFirstResult() {
        return selectResult(0);
    }

    public ItemPage selectResult(int index) {
        boolean resutsStatus = verifyResults();

        if (results.size() <= 0) {
            throw new NoSuchElementException("No results.");
        }

        if (index < 0 || index >= results.size()) {
            throw new IndexOutOfBoundsException("Index must be from 0 to results list size, currentIndex=" + index + ", resultsSize=" + results.size());
        }

        WebElement currentItem = null;

        currentItem = getItemFromResultsList(index, resutsStatus);

        wait.until(ExpectedConditions.elementToBeClickable(currentItem));
        currentItem.click();
        return new ItemPage(driver);
    }

    private WebElement getItemFromResultsList(int index, boolean resultsStatus) {
        WebElement currentItem;

        if (resultsStatus) {
            currentItem = results.get(index).findElement(By.className("g-i-tile-i-title"));
        } else {
            currentItem = results.get(index).findElement(By.className("g-i-list-title"));
        }

        return currentItem;
    }

    private boolean verifyResults() {
        results = driver.findElements(By.xpath("//div[@class='rz-goods-main-content clearfix']//div[@name='search_list']/div[@class='g-i-tile g-i-tile-catalog']"));
        if (results.size() == 0) {
            results = driver.findElements(By.xpath("//div[@class='g-i-search-list']//div[@class='g-i-list available clearfix']"));
            return false;
        }

        return true;
    }
}
