package ua.com.testmatick.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageObjectRozetkaBase {

    @FindBy(xpath = "//div[@id='rz-search']//input[@name='text']")
    private WebElement inputField;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public ResultsPage searchFor(String message) {
        inputField.sendKeys(message);
        inputField.submit();

        return new ResultsPage(driver);
    }
}
